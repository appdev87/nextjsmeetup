import { Fragment } from 'react';

import { MongoClient } from 'mongodb';
import MeetupList from '../components/meetups/MeetupList';
import Head from 'next/head';


const HomePage = (props) => {

  return (
    <Fragment>
      <Head>
        <title>React Meetups</title>
        <meta name="description" content="Browse a huge list of highly active React Meetups"/>
      </Head>
      <MeetupList meetups={props.meetups}/>
    </Fragment>
  );
};

// /**
//  * Used on the server to fetch new data. Use only one of the two, either getServerProps or getStaticProps
//  * @returns {Promise<{props: {meetups: [{image: string, address: string, description: string, id: string, title: string},{image: string, address:
//  *   string, description: string, id: string, title: string}]}}>}
//  */
// export async function getServerProps(context) {
//   const req = context.req;
//   const res = context.res;
//
// // fetch data
//   return {
//     props: {
//       meetups: DUMMY_MEETUPS
//     }
//   }
// }

/**
 * Function called by nextJS to preload the data beforehand.
 * @returns {Promise<{revalidate: number, props: {meetups: {image: *, address: *, id: *, title: *}[]}}>}
 */
export async function getStaticProps() {
// fetch data

  const client = await MongoClient.connect(
    'mongodb+srv://appdevadmin:appdevadmin@cluster0.hefmc.mongodb.net/meetupsapp?retryWrites=true&w=majority'
  );
  const db = client.db();

  const meetupsCollection = db.collection('meetups');
  const meetups = await meetupsCollection.find()
                                         .toArray();

  await client.close();

  return {
    props: {
      meetups: meetups.map(meetup => ({
        title: meetup.title,
        address: meetup.address,
        image: meetup.image,
        id: meetup._id.toString()
      }))
    },
    revalidate: 1 // Number of seconds to wait before reloading the data and fetch new requests
  };
}

export default HomePage;
