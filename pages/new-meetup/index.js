import { useRouter } from 'next/router';
import { Fragment } from 'react';
import Head from 'next/head';


import NewMeetupForm from '../../components/meetups/NewMeetupForm';

const NewMeetupPage = () => {
  const router = useRouter();

  const addMeetupHandler = async (enteredMeetupData) => {
    console.log(enteredMeetupData);

    const response = await fetch('/api/new-meetup', {
      method: 'POST',
      body: JSON.stringify(enteredMeetupData),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .catch((error) => {console.error(error);});


    const data = await response.json();
    console.log(data);

    await router.push('/');
  };

  return (
    <Fragment>
      <Head>
        <title>New Meetup</title>
        <meta
          name="description"
          content="Adding new meetup has never been so easy"
        />

      </Head>
      <NewMeetupForm onAddMeetup={addMeetupHandler}/>
    </Fragment>
  );
};

export default NewMeetupPage;
