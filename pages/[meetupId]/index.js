import Head from 'next/head';
import { Fragment } from 'react';
import { MongoClient, ObjectId } from 'mongodb';

import MeetupDetailPage from '../../components/meetups/MeetupDetail';

const MeetupDetailsPage = ({ meetupData }) => {
  const { image, title, address, description } = meetupData;
  console.log(meetupData);

  return (
    <Fragment>
      <Head>
        <title>{title}</title>
        <meta name="description" content={description}/>
      </Head>
      <MeetupDetailPage
        image={image}
        title={title}
        address={address}
        description={description}
      />
    </Fragment>
  );
};

export async function getStaticPaths() {

  const client = await MongoClient.connect(
    'mongodb+srv://appdevadmin:appdevadmin@cluster0.hefmc.mongodb.net/meetupsapp?retryWrites=true&w=majority'
  );
  const db = client.db();

  const meetupsCollection = db.collection('meetups');
  const meetups = await meetupsCollection.find({}, { _id: 1 })
                                         .toArray();

  await client.close();

  return {
    paths: meetups.map((meetup) => (
      {
        params:
          { meetupId: meetup._id.toString() }
      }
    )),
    fallback: 'blocking'
  };
}

export async function getStaticProps(context) {
  const meetupId = context.params.meetupId;

  const client = await MongoClient.connect(
    'mongodb+srv://appdevadmin:appdevadmin@cluster0.hefmc.mongodb.net/meetupsapp?retryWrites=true&w=majority'
  );
  const db = client.db();

  const meetupsCollection = db.collection('meetups');
  const selectedMeetup = await meetupsCollection.findOne({ _id: ObjectId(meetupId) });

  await client.close();

  return {
    props: {
      meetupData: {
        id: selectedMeetup._id.toString(),
        title: selectedMeetup.title,
        address: selectedMeetup.address,
        image: selectedMeetup.image,
        description: selectedMeetup.description
      }
    }
  };
}

export default MeetupDetailsPage;